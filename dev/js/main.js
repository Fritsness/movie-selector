/* global MovieProxy, Dom, Utils */
(function (MovieProxy, Dom, Utils) { // eslint-disable-line no-unused-vars

/*  INIT
================================================================== */
  window.addEventListener('DOMContentLoaded', function (e) {
    MovieProxy.getRandomMovie()
      .then(r => {
        return MovieProxy.getMoviePoster('w500')(r.poster_path)
        .then(resp => {
          r.fullPosterPath = resp.url;
          return r;
        });
      })
      .then(r => {
        const setResp = Dom.setResponse(Dom.resultsList);
        // const [movieData, poster_path]
        setResp({tag: 'h2', content: r.title});
        setResp({tag: 'p', content: r.overview});
        setResp({tag: 'img', props: {width: 500, src: r.fullPosterPath}});

        // Dom.setDate(Dom.resultsList)
      });
  });

  Dom.searchForm.addEventListener('submit', function (e) {
    e.preventDefault();
    var title = Dom.searchInput.value.toString().trim();

    MovieProxy.getMovieByTitle(title).then(function(resp) {
    });
  });
}(MovieProxy, Dom, Utils));
