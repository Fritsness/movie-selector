/* global Utils API_KEY*/
var MovieProxy = (function proxy (Utils, API_KEY) { // eslint-disable-line no-unused-vars

  let config = {};
  var baseURL = 'https://api.themoviedb.org/3';
  const queryMethods = {
    find: 'find',
    config: 'configuration',
    discover: 'discover'
  };

  const defaultQueryParams = {
    api_key: API_KEY.key()
  };

  function createQueryString(queryObject) {
    return Object.keys(queryObject).reduce((acc, curr, index) => {
      // prepends '&' to the string if its not the first parameter
      const separator = index ? '&' : '';
      return acc.concat(`${separator}${curr}=${queryObject[curr]}`);
    }, '');
  }

  function getConfig () {
    return fetch(`${baseURL}/${queryMethods.config}?${createQueryString(defaultQueryParams)}`)
      .then(resp => resp.json())
      .then(resp => config = resp);
  }

  /**
   * Getting a movie by its IMDB ID
   * @param  {String} imdbId  - an IMDB ID format ttXXXXXXX
   * @return {Object} resp    - the movie object returned by the server
   */
  function getMovieByImdbId (imdbId) {
    const queryParams = createQueryString(Object.assign(
      {},
      defaultQueryParams,
      {
        external_source: 'imdb_id',
        language: 'fr-BE'
      }
    ));
    return fetch(`${baseURL}/${queryMethods.find}/${imdbId}?${queryParams}`).then(function(resp) {
      const r = resp.json();
      return r;
    }).catch(function(err) {
      return err;
    });
  }


// TODO: try a more performant implementation.
// see https://www.themoviedb.org/talk/57e2dd4d92514101c8001fb8
  function getRandomMovie () {
    const pageSeed = Math.floor(Utils.getRandomArbitrary(1, 19));
    const numberSeed = Math.floor(Utils.getRandomArbitrary(1, 19));
    const queryParams = createQueryString(Object.assign(
      {},
      defaultQueryParams,
      {
        language: 'fr-FR',
        include_adult: false,
        'vote_average.gte': 5,
        page: pageSeed,
      }
    ));

    const URL = `${baseURL}/${queryMethods.discover}/movie?${queryParams}`;
    return fetch(URL)
      .then(r => {
        const resp = r.json();
        return resp;
      })
      .then(r => {
        return r.results[numberSeed];
      });
  }

  function getMoviePoster(fileSize) {
    const BASE_PATH = 'https://image.tmdb.org/t/p/';
    return function (filePath) {
      const URL = BASE_PATH + fileSize + filePath;
      return fetch(URL).then(resp => {
        return resp;
      });
    };
  }

  return {
    // getMovieByTitle,
    getRandomMovie,
    getMovieByImdbId,
    getConfig,
    getMoviePoster
  };

}(Utils, API_KEY));
